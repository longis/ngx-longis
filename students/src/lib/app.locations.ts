export const STUDENTS_APP_LOCATIONS = [
  {
    'privilege': 'Location',
    'help': 'Enables student department features',
    'target': {
      'url': '^/department'
    },
    'mask': 0
  },
  {
    'privilege': 'Location',
    'help': 'Enables student semester features like calculating current semester',
    'target': {
      'url': '^/semester'
    },
    'mask': 0
  },
  {
    'privilege': 'Location',
    'target': {
      'url': '^/auth/'
    },
    'mask': 1
  },
  {
    'privilege': 'Location',
    'target': {
      'url': '^/error'
    },
    'mask': 1
  },
  {
    'privilege': 'Location',
    'account': {
      'name': 'Students'
    },
    'target': {
      'url': '^/'
    },
    'mask': 1
  }
];
