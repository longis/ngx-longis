const en = {
        "AttachmentTypes": {
          "Title": "File types"
        },
        "Candidates": {
          "Title": "Student Pre-Registration application periods",
          "TitleSingular": "Student Pre-Registration application period",
          "Students": "Candidate students",
          "RequestAttachmentsHelp": "Candidate students must submit the selected documents before final submission of the pre-registration application.",
          "TitleHelp": "Set the title that will be visible to students when submitting applications",
          "PreviewStudent": "Student preview",
          "StudentDetails": "Student details",
          "StudentsSingular": "Student",
          "StudentsPlural": "Students",
          "RemoveEventMessage": {
            "title": "Delete student Pre-Registration periods"
          },
          "Edit": {
            "CommunicateWithAttendees": {
              "Title": "Communicate with students",
              "Description": "Please fill in the subject and body of the message you want to send to the students and press send"
            }
          },
          "InscriptionModesHelp": "Set the admission modes allowed for candidate students to apply for this study program. Leave blank if all admission modes are allowed",
          "CreateRegisterAction": {
            "Description": "The application creation process will attempt to create applications for the selected registrations. The process can be completed for candidate students without an application who have an account."
          },
          "CreateGuestUserAction": {
            "Description": "The guest account creation process will attempt to create accounts for the selected registrations. The process can be completed for candidate students without an account."
          },
          "SendActivationMessage": {
            "Description": "The text message sending process will attempt to forward the account activation details to the candidates. The process can be completed for candidate students with an account and a mobile phone."
          },
          "DeleteAction": {
            "Title": "Delete candidate students"
          },
          "CreateStudent": {
            "Title": "Register as an active student",
            "Description": "Attention: With this action, you can directly admit the selected candidates as active students without a pre-registration application. Admission can only occur if the candidate has not already been admitted and there is no pending pre-registration application for the academic year of admission."
          },
          "CandidateListInfo": "The list below shows the candidate students of the departments you have access to",
          "ImportAction": {
            "Title": "Import candidate students from *.xlsx File",
            "Help": "Upload the *.xlsx file of candidate students below, select the source you have connected with the *.json configuration file, and press \"Import\" to start the process. You can export the *.xlsx file from a data source using the \"Export File\" button.",
            "SuccessToast": {
              "Message": "The (asynchronous) import process of candidate students has started successfully."
            }
          },
          "ExportAction": {
            "Title": "Export Candidate Students File"
          },
          "RelatedStudent": "Linked with student",
          "UploadActions": {
            "Title": "Candidate student import actions",
            "TitleSingular": "Candidate student import action",
            "InscriptionYear": "Student registration year",
            "SuccessfulNoUpdates": "Import all students from the file",
            "Total": "Total Candidate students",
            "NoResults": "No failed student imports/updates found for this action."
          },
          "ArticlesHelp": "Specify which articles you want to appear to the candidate students depending on their application status.",
          "ArticlesInfo": "The following articles will be shown to the candidate students depending on their application status",
          "NoArticles": "No articles have been set to appear to candidate students."
        },
        "Classes": {
          "MaxNumberOfStudents": "Maximum number of students",
          "MinNumberOfStudents": "Minimum number of students",
          "NumberOfStudents": "Number of students",
          "MinMaxStudentNumber": "Minimum - Maximum Number of students",
          "RegisteredStudents": "Registered students",
          "Students": "Students",
          "AddStudent": "Add student",
          "AddStudentsMessage": {
            "title": "Add student to class",
            "one": "One new student added successfully.",
            "many": "{{value}} new students added successfully."
          },
          "RemoveStudentsMessage": {
            "title": "Remove student from class",
            "one": "One student removed successfully.",
            "many": "{{value}} students removed successfully."
          },
          "RemoveStudentTitle": "Remove student",
          "RemoveStudentMessage": "You are about to delete one or more students from the class. Do you want to proceed?",
          "WithStudentLimit": "With student Limit",
          "OpenAction": {
            "Description": "With this action, you can change the status of the selected classes to open. An open class can be enrolled in by students who meet its enrollment prerequisites."
          },
          "CloseAction": {
            "Description": "With this action, you can change the status of the selected classes to closed. A closed class cannot be enrolled in by students."
          },
          "DeleteAction": {
            "Description": "With this action, you can delete the selected classes. Deletion can only occur for classes that have not been enrolled in by students.",
            "CompletedWithErrors": {
              "Description": {
                "One": "One class was not deleted either due to an error or because there are student enrollments",
                "Many": "{{errors}} classes were not deleted either due to an error or because there are student enrollments."
              }
            }
          },
          "DepartmentStudents": "Department students",
          "StudentsTotal": "Total students",
          "OtherDepartmentClasses": "The following list shows the classes of other departments registered by students of your department",
          "AddToSectionManually": {
            "Title": "Add/register student to a section",
            "Description": "With this action, you can add/register students to the selected class section. The entry requirements for the section (if any) will be checked, as well as the maximum number of students allowed to register. Students must belong to the departments you manage.",
            "CompletedWithErrors": {
              "Description": {
                "One": "One student was not added to the section due to an error.",
                "Many": "{{errors}} students were not added to the section due to an error."
              }
            }
          },
          "RemoveFromSection": {
            "Description": "With this action, you can remove the selected students from the class section they are in. Students must belong to the departments you manage.",
            "CompletedWithErrors": {
              "Description": {
                "One": "One student was not removed from the section due to an error.",
                "Many": "{{errors}} students were not removed from the section due to an error."
              }
            }
          },
          "EditStudentClassStatus": {
            "Description": "With this action, you can change the class registration status for the selected students, provided they are active and belong to the departments you manage.",
            "CompletedWithErrors": {
              "Description": {
                "One": "The class registration status of one student was not changed due to an error.",
                "Many": "The class registration status of {{errors}} students was not changed due to an error."
              }
            }
          },
          "CalculateStatisticsAction": {
            "Description": "With this action, you can calculate statistics for the selected classes such as the number of students who registered for the course, how many were examined, and how many received a passing grade. The calculation is recommended to be done after the associated exam periods of the class have ended."
          },
          "RemoveStudentsFromClass": {
            "Title": "Remove students from class",
            "Description": "With this action, you can remove the selected students from the class. This can only be done for active students of your departments who have not been graded in any exam of the course and for whom no absences have been recorded in the class. Also, their registrations for the specific academic cycle should not be closed. This action does NOT check if the semester registration rules for the students are valid after the removal.",
            "CompletedWithErrors": {
              "Description": {
                "One": "One student was not removed from the class due to an error or because you do not have the right to perform the action.",
                "Many": "{{errors}} students were not removed from the class due to an error or because you do not have the right to perform the action."
              }
            }
          }
        },
        "Counselors": {
          "ListTitle": "Counselor-student connections",
          "ListTitleSingular": "Counselor-student connection",
          "Student": "Student",
          "DeleteAction": {
            "Description": "With this action, you can delete the selected academic counselor-student connections."
          },
          "DisableAction": {
            "Description": "With this action, you can disable the selected academic counselor-student connections and set the end of counseling details."
          },
          "StudentCounselors": "Student academic counselors"
        },
        "Courses": {
          "NoStudents": "No students available",
          "RegisteredStudents": "Registered students",
          "RegisteredStudent": "Registered student",
          "Students": "Students",
          "Lists": {
            "GradeScale": {
              "LongDescription": "Manage the grading scales used for course grades, student assignments, etc."
            }
          },
          "ChangeCourseAttributesAction": {
            "Description": "With this action, you can change the properties (ects, DM, semester, etc.) of the specific course for the selected students. Changes can only be made for active students.",
            "CompletedWithErrors": {
              "Description": {
                "One": "The course properties were not changed for one student due to an error.",
                "Many": "The course properties were not changed for {{errors}} students due to an error."
              }
            }
          },
          "ResultsPlural": "course attribute changes for students",
          "ResultsSingular": "course attribute change for a student",
          "ChangeGradeScaleAction": {
            "ScaleHasBeenUsedForGrading": "The course grading scale cannot be changed as it has been used to calculate at least one student grade.",
            "Info": "Note: The process of changing the grading scale can only be carried out for courses where no student grade has been calculated (e.g., in grade submissions, grade exemption entry, etc.). If the course is composite, this condition must also be met by all its parts. Likewise, if it is part of a composite, this must apply to the other parts and the parent course."
          }
        },
        "Settings": {
          "Lists": {
            "GradeScale": {
              "LongDescription": "Manage the grading scales used for course grades, student assignments, etc."
            },
            "Company": {
              "LongDescription": "Manage the companies and organizations that offer internships for students."
            },
            "StudyProgramRegisterActions": {
              "Description": "Student application requests",
              "LongDescription": "Manage the list of prospective students by accepting or rejecting their applications."
            }
          }
        },
        "Dashboard": {
          "ActiveStudents": "Active students",
          "RegistrarAssistantModuleTitle": "Registrar Assintant for Study Programs",
          "RegistrarModuleTitle": "Life Long Learning Administration"
        },
        "DegreeTemplates": {
          "SetDegreeTemplateDescription": "Matching a degree template with one or more directions will enable the eDiplomas service to receive the degree details for a graduate student of the corresponding direction. Programs' directions that are not matched become inactive for degree issuance processes.",
          "SetStudentDegreeTemplateDescription": "Matching a degree template with one or more students will enable the eDiplomas service to receive the degree details for a graduate student. This process overrides the matching based on the student's program direction."
        },
        "Departments": {
          "UsersListInfo": "The list below shows the users who have or can obtain rights to edit various components of the organizational unit, such as editing student details and executing actions like printing reports."
        },
        "Snapshots": {
          "Info": "You can keep historical data of the organizational unit in its history. To link students with the historical data, go to the student list, select the students you want, and execute the process of linking with the organizational unit's history."
        },
        "Documents": {
          "Lists": {
            "DepartmentDocumentNumberSeries": {
              "LongDescription": "Manage the available document series used for archiving documents such as student certificates, exam grades, etc."
            },
            "InstituteDocumentNumberSeries": {
              "LongDescription": "Manage the available document series of the institution used for central archiving of documents such as student certificates, exam grades, etc."
            }
          }
        },
        "DataAvailabilityAttributes": {
          "Validation": {
            "Title": "Service Check for Graduate Degree Retrieval",
            "Description": "Use the form below to enter the details of the student you want to check within the data retrieval service."
          },
          "NotSupported": "The graduate degree retrieval service is not enabled. Managing the availability of graduate student data is not possible.",
          "Update": {
            "Message": "You are about to update the selected data according to the existing graduate student records. Do you want to continue?"
          },
          "Description": "Use the data availability to update applications and services regarding the status of graduate student data across all academic years."
        },
        "RestoreStudents": {
          "Info": "Use the restoration of deleted students to reinstate students who have been removed from the student registry.",
          "Restore": {
            "Title": "Restore students",
            "Description": "Attention: With this action, you can reinstate the selected entries as students. Restoration can only be done once, and you can choose whether to reassign registry numbers or not.",
            "CompletedWithErrors": {
              "Description": {
                "One": "One record was not entered due to an error or because a student with the same registry number already exists in the department",
                "Many": "{{errors}} records were not entered due to an error or because a student with the same registry number already exists in the department"
              }
            }
          }
        },
        "Exams": {
          "NumberOfGradedStudents": "Graded students",
          "Students": "Students",
          "MinNumberOfGradedStudents": "Minimum number of Graded students",
          "MaxNumberOfGradedStudents": "Maximum number of Graded students",
          "OpenAction": {
            "Description": "With this action, you can change the status of the selected exams to open. An open exam can be modified by the instructors with the submission of student grades.",
            "DescriptionOne": "With this action, you can change the status of the exam to open. An open exam can be modified by the instructors with the submission of student grades."
          },
          "DeleteAction": {
            "Description": "With this action, you can delete the selected exams. Deletion can only be done for exams without graded students.",
            "CompletedWithErrors": {
              "Description": {
                "One": "One exam was not deleted either due to an error or because there are student grades",
                "Many": "{{errors}} exams were not deleted either due to an error or because there are student grades."
              }
            }
          },
          "UploadGradingScore": "Upload the .xlsx file of student grades. You can submit a grade sheet with blank grades, as well as delete existing grades by putting a dash (\"-\") in the corresponding column.",
          "GradeStatusTypes": {
            "ESTATUS": "The student is not active",
            "EAVAIL": "The student is not available for this exam. Ensure that their details have not been changed in the file.",
            "E_DENY_GRADING": "Grading the student is not allowed (e.g., due to insufficient attendance)"
          },
          "CalculateStatisticsAction": {
            "Description": "With this action, you can calculate statistics for the selected exams such as the number of students eligible to take the exam, how many were examined, and how many received a passing grade."
          },
          "StatisticsHelp": "To recalculate statistics such as the number of students eligible to take the exam, how many were examined, and how many received a passing grade, select the exams you want and execute the \"Exam Statistics Calculation\" action."
        },
        "Grades": {
          "NumberOfGradedStudents": "Graded students",
          "Students": "Students"
        },
        "Graduations": {
          "SearchPlaceholder": "Title of graduation, academic year, etc.",
          "GraduationDate": "Graduation Date",
          "Students": "Students",
          "Info": "Graduation Information",
          "NewGraduation": "Create Graduation Event",
          "EditGraduation": "Edit Graduation Event",
          "GraduationTime": "Graduation Event Time",
          "RequestAttachments": "The following list includes all available document types for submission. Select those you wish to include in the graduation requests before submission.",
          "RequestAttachmentsHelp": "Students must submit the selected documents before final submission of the graduation request.",
          "AutoRequestDocuments": "The following list includes all available reference templates related to student certification requests. Select those you wish to automatically generate requests for upon completion of the process.",
          "AutoRequestDocumentsHelp": "Students will be able to review or receive the documents once generated.",
          "TitleHelp": "Set the title of the event visible to students during application submission.",
          "AcademicYearPeriodHelp": "Select the academic year and cycle of graduation.",
          "LocationHelp": "Set the location where the event will take place.",
          "AttachmentTypesTitle": "Documents the student must submit.",
          "ReportTemplatesTitle": "Documents students will receive upon completion of the process.",
          "GraduationLocation": "Graduation Event Location",
          "PreviewStudent": "Preview student",
          "StudentDetails": "Student Details",
          "StudentsSingular": "Student",
          "StudentsPlural": "Students",
          "UrlHelp": "Set the web location where the event will take place.",
          "Error": "An error occurred while saving.",
          "GraduationDateRequired": "The graduation event date has not been filled in.",
          "CheckDates": "Check the application period dates and the graduation event date.",
          "Edit": {
            "Title": "Graduation Requests",
            "CommunicateWithAttendees": {
              "Title": "Communicate with students",
              "Description": "Please fill in the subject and body of the message you want to send to the students and click send."
            }
          },
          "AddStudents": {
            "Title": "Create Graduation Requests",
            "many": "{{value}} new graduation requests created successfully.",
            "one": "One new graduation request created successfully."
          },
          "AddRequestsFromResults": {
            "Description": "You can add graduation requests for active students who meet diploma requirements by selecting the date the graduation check was performed.",
            "many": "{{value}} graduation requests created successfully.",
            "one": "One graduation request created successfully.",
            "CompletedWithErrors": {
              "Description": {
                "One": "A graduation request for one student was not completed either because it already exists or because the graduation requirements are not met.",
                "Many": "Graduation requests for {{errors}} students were not completed either because they already exist or because the requirements are not met."
              }
            }
          },
          "RequestDeleteAction": {
            "Description": "With this action, you can delete an active graduation request created by you. Deletion can ONLY be done for active requests that are not linked to other actions and do not contain documents.",
            "CompletedWithErrors": {
              "Description": {
                "One": "A graduation request was not deleted either due to an error or because it is linked to other actions.",
                "Many": "{{errors}} graduation requests were not deleted either due to an error or because they are linked to other actions."
              }
            }
          },
          "Alerts": {
            "CanCreateRequest": "You can add new requests to the graduation event."
          },
          "RequestTitle": "Graduation Request from the Secretariat",
          "RequestDescription": "Graduation Request from the Secretariat",
          "CompleteGraduation": "Complete Graduation",
          "CancelGraduation": "Cancel Graduation",
          "CompleteDeclarationAction": {
            "Title": "Complete student Approval",
            "Description": "With this action, you can complete the declaration of selected students, provided their declaration action is pending (i.e., not completed/canceled in the past). The process is identical to the declaration completion through the graduation request.",
            "CompletedWithErrors": {
              "Description": {
                "One": "The declaration for one student was not completed due to an error.",
                "Many": "Declarations for {{errors}} students were not completed due to errors."
              }
            }
          },
          "CancelDeclarationAction": {
            "Title": "Cancel student Approval",
            "Description": "With this action, you can cancel the approval of selected students and revert them to active status, provided their declaration action is pending (i.e., not completed/canceled in the past). The process is identical to canceling the declaration through the graduation request.",
            "CompletedWithErrors": {
              "Description": {
                "One": "The declaration for one student was not canceled due to an error.",
                "Many": "Declarations for {{errors}} students were not canceled due to errors."
              }
            }
          },
          "CompleteGraduationAction": {
            "Title": "Complete Graduation",
            "Description": "With this action, you can complete the graduation of selected students (and designate them as graduates), provided their graduation action is pending (i.e., not completed/canceled in the past). The process is identical to graduation through the graduation request. After successful completion, you can view the students in the 'Graduates' tab.",
            "CompletedWithErrors": {
              "Description": {
                "One": "The graduation for one student was not completed due to an error.",
                "Many": "Graduations for {{errors}} students were not completed due to errors."
              }
            }
          },
          "CancelGraduationAction": {
            "Title": "Cancel Graduation",
            "Description": "With this action, you can cancel the graduation of selected students, provided their graduation action is pending (i.e., not completed/canceled in the past). The process is identical to canceling the graduation through the graduation request.",
            "CompletedWithErrors": {
              "Description": {
                "One": "The graduation for one student was not canceled due to an error.",
                "Many": "Graduations for {{errors}} students were not canceled due to errors."
              }
            }
          },
          "SetLastObligationDateAction": {
            "Description": "With this action, you can mass-set the last obligation date for students.",
            "CompletedWithErrors": {
              "Description": {
                "One": "The last obligation date was not saved for one student due to an error or because it is not valid.",
                "Many": "Last obligation dates for {{errors}} students were not saved due to errors or because they are not valid."
              }
            }
          },
          "ResetToDeclaredAction": {
            "Title": "Cancel Graduation",
            "Description": "With this action, you can cancel the graduation of selected students and revert them to the declaration status. The process can only be performed for students associated with completed graduation actions. If you wish for the students to become graduates again, you must use the 'Complete student Approval' action found in their view actions list.",
            "CompletedWithErrors": {
              "Description": {
                "One": "The graduation for one student was not canceled due to an error.",
                "Many": "Graduations for {{errors}} students were not canceled due to errors."
              }
            }
          },
          "CalculateRankByGradYear": {
            "Title": "Calculate Percentage Rank by Graduation Year",
            "Description": "With this action, you will calculate the percentage rank of selected students based on their graduation year. This process can only be performed for graduate students.",
            "CompletedWithErrors": {
              "Description": {
                "One": "Calculation of percentage rank for one student could not be completed due to an error.",
                "Many": "Calculation of percentage rank for {{errors}} students could not be completed due to errors."
              }
            }
          },
          "CalculateRankByInscrYear": {
            "Description": "With this action, you will calculate the percentage rank of selected students based on their enrollment year. This process can only be performed for graduate students.",
            "CompletedWithErrors": {
              "Description": {
                "One": "Calculation of percentage rank for one student could not be completed due to an error.",
                "Many": "Calculation of percentage rank for {{errors}} students could not be completed due to errors."
              }
            }
          },
          "CalculateClassesPercentileRank": {
            "Description": "With this action, you will start calculating the percentile rank of selected students' course grades. Completion of calculations may take a few minutes. You can monitor the results in the 'Calculate Percentile Ranks' tab.",
            "CompletedWithErrors": {
              "Description": {
                "One": "Calculation of percentile rank for one student could not be completed due to an error.",
                "Many": "Calculation of percentile rank for {{errors}} students could not be completed due to errors."
              }
            },
            "AlreadyCalculated": {
              "Message": "The percentile ranks of course grades for all selected students have already been calculated."
            },
            "OnlyCompleted": {
              "Message": "Calculation of percentile ranks for course grades can only be performed for graduate students who belong to completed graduation events. Use the 'Change Status' action to complete the event."
            }
          },
          "FullName": "Student Full Name",
          "StudentUserName": "Student Username",
          "ClearDatesMessage": "Set the application period dates to make the event available to students, otherwise it will only be visible to the administration."
        },
        "Instructors": {
          "SupervisedStudents": "Σπουδαστές υπό επίβλεψη"
        },
        "Internships": {
          "StudentIdentifier": "Student ID Number",
          "StudentStatus": "Student Status",
          "PreviewStudent": "Preview student",
          "Students": "Students",
          "Student": "Student",
          "active-active-students": "Active (Active students)",
          "NoStudentMessage": "This internship has not been taken by any student.",
          "DeleteAction": {
            "Description": "With this action, you can delete the internship. The deletion of the internship is allowed only if it has not been completed and the student is active.",
            "CompletedWithErrors": {
              "Description": {
                "One": "One internship was not deleted either due to an error, its status, or the student's status.",
                "Many": "{{errors}} internships were not deleted either due to errors, their status, or the students' status."
              }
            }
          }
        },
        "Messages": {
          "toStudents": {
            "BodyPlaceHolder": "Message to the student"
          }
        },
        "Register": {
          "AcceptConfirm": {
            "Message": "You are about to accept the application. After this, the candidate will become an active student of the selected study program. Do you want to continue?"
          }
        },
        "Registrations": {
          "StudentStatus": "Student Status",
          "StudentName": "Student Name",
          "ListTable": {
            "Student": {
              "label": "Student Name",
              "ID": "Student ID"
            },
            "Preview": {
              "Student": "Preview student"
            }
          },
          "AdvancedSearch": {
            "Student": "Student",
            "StudentID": "Student ID"
          },
          "OpenAction": {
            "Description": "With this action, you can change the status of selected course declarations to open. An open declaration can be modified by students during the declaration period. This action can only be performed for closed declarations and active students."
          },
          "ChangePeriodRegistrationTypeAction": {
            "NoItems": "The selected declarations must have an open status and belong to active students."
          },
          "ChangePeriodRegistrationType": {
            "message": "The process of smart selection of all elements requires a filter for student status, status, and registration period type of course declarations. Use the filter and set values for the above fields. Press the 'Search' button to refresh the details."
          },
          "EditRegistration": {
            "WarningNotActiveStudent": "You cannot edit the declaration because the student is not active."
          },
          "DeleteAction": {
            "Description": "With this action, you can delete the selected declarations. Only declarations of active students that contain no courses and have not been edited by the student can be deleted.",
            "CompletedWithErrors": {
              "Description": {
                "One": "One declaration was not deleted due to an error. Note that the declaration must contain no courses and must not have been edited by the student.",
                "Many": "{{errors}} declarations were not deleted due to errors. Note that the declarations must contain no courses and must not have been edited by the students."
              }
            }
          },
          "AutoRegisterClassesAction": {
            "DescriptionRegistrations": "With this action, you can automatically add courses to selected declarations based on the criteria you define. This action can only be performed for open declarations and active students.",
            "DescriptionStudents": "With this action, you can create declarations for the current academic cycle for selected students and automatically add courses based on the criteria you define.",
            "CompletedWithErrorsStudents": {
              "Description": {
                "One": "One student declaration was not created/modified due to an error that occurred during the process initialization.",
                "Many": "{{errors}} student declarations were not created/modified due to errors that occurred during the process initialization."
              }
            }
          },
          "ActiveStudents": "Students without declaration",
          "CreateRegistrationAction": {
            "Description": "With this action, you can create (empty) declarations for the current academic year-cycle for selected students. This action can only be completed for students who do not have registration for the current academic year-cycle.",
            "Info": "The list below shows the active students of the organic unit who do not have a declaration for the current academic cycle. You can create empty declarations for them or start an automatic declaration process using the actions menu.",
            "RegistrationExists": "There is already a semester declaration for the selected academic year and cycle."
          }
        },
        "Requests": {
          "Student": "Student",
          "StudentStatus": "Student Status",
          "Edit": {
            "StudentDetails": "Student Details",
            "ActivateAction": {
              "GraduationRequestActionsMessage": "After the end of the process, you will be able to approve or reject the graduation request again. Do you want to continue?"
            }
          },
          "PublishDocumentNoUser": "It seems that the account of the student has been deleted. Publishing the document cannot be performed.",
          "ValidateSpecialtyRulesAction": {
            "Description": "The process will verify the specialty selection prerequisites of the students of the selected pending requests. This is the necessary first step for their bulk approval."
          },
          "PreferredSpecialtyRequestAction": {
            "Accept": {
              "Description": "The process will attempt to complete the selected pending requests and move the students to their preferred specialty. It can only be used for those who meet the necessary prerequisites (first perform the 'Validate Specialty Selection Prerequisites' action). Note: In special cases, you can individually approve requests from their 'View'."
            }
          }
        },
        "Forms": {
          "GraduationRequestAction": "Graduation Request",
          "StudentAttributeRuleTitle": "Student Attribute",
          "category": "Student Category",
          "graduationPeriod": "Academic Cycle of Graduation",
          "graduationYear": "Academic Year of Graduation"
        },
        "GraduationRequestActions": {
          "GraduationRules": "Check Graduation Requirements",
          "Success": "Graduation requirements check successful",
          "Warning": "Graduation requirements check unsuccessful",
          "DeclareStudentMessage": "You are about to declare the student. Certificate requests will be generated automatically if defined in the graduation event. Do you wish to continue?",
          "CancelDeclareStudentMessage": "You are about to cancel the student declaration. Additionally, the graduation request will be automatically set to active. Do you wish to continue?",
          "CompleteDeclareStudent": "Student declaration completed successfully",
          "CancelCompleteDeclareStudent": "Cancellation of student declaration and restoration to active status completed successfully",
          "GraduationInfo": "Graduation Information",
          "GraduateStudent": "Graduate student",
          "GraduateStudentMessage": "You are about to graduate the student. The student will now be a graduate and you will not be able to edit their details. Do you wish to continue?",
          "CancelGraduateStudent": "Cancel Graduation",
          "CancelGraduateStudentMessage": "You are about to cancel the student's graduation. The student will remain pending graduation. Do you wish to continue?",
          "CompleteGraduateStudent": "Student status changed to graduate successfully",
          "CancelCompleteGraduateStudent": "Cancellation of student graduation completed successfully",
          "ChangeStatusPotentialInfo": "If any of the attached documents are incorrect and you wish for the student to re-edit the request, click here",
          "MessageMailSubject": "Update regarding your graduation request"
        },
        "Rules": {
          "StudentRule": {
            "Title": "Student Attribute"
          },
          "RuleTypes": {
            "CourseClassSectionRules": "Student Admission Requirements to the Department"
          }
        },
        "Scholarships": {
          "TotalStudents": "Total students",
          "Student": "Student",
          "Students": "Students",
          "AddStudent": "Add student",
          "RemoveStudent": "Remove student",
          "AddStudentsMessage": {
            "title": "Add student to Scholarship",
            "one": "One new student added successfully.",
            "many": "{{value}} new students added successfully."
          },
          "RemoveStudentsMessage": {
            "title": "Remove student from Scholarship",
            "one": "One student removed successfully.",
            "many": "{{value}} students removed successfully."
          },
          "RemoveStudentTitle": "Remove student",
          "RemoveStudentMessage": "You are about to delete one or more scholarship students. Do you wish to proceed?",
          "AddStudents": "Add students",
          "AddStudentsAction": {
            "Title": "Add students to Scholarship",
            "Description": "With this action, selected students will be added to the scholarship if they meet the conditions",
            "CompletedWithErrors": {
              "Description": {
                "One": "One student was not transferred to the scholarship either because they do not meet the conditions or due to an error.",
                "Many": "{{errors}} students were not transferred to the scholarship either because they do not meet the conditions or due to an error."
              }
            }
          },
          "CheckRulesHelp": "To perform a scholarship requirements check, go to the list of students, select the students you wish, and execute the requirements check process for the specific scholarship"
        },
        "Students": {
          "StudentName": "Student Name",
          "StudentTitle": "Students",
          "StudentTitleSingular": "Student",
          "NewStudent": "Create student",
          "StudentId": "Student Code",
          "Student": "Student",
          "DeclaredStudents": "Declared students",
          "GraduationYear": "Academic Year of Graduation",
          "GraduationPeriod": "Academic Cycle of Graduation",
          "GraduationDate": "Graduation Date",
          "StudentCategory": "Student Category",
          "EditStudentCourse": "Edit student Course",
          "RemoveCourseTitle": "Remove student Course",
          "RemoveCourseMessage": "You are about to delete the course from the student. Do you want to continue?",
          "CalculateSemesterAction": {
            "Title": "Calculate student Semester",
            "Description": "With this action, you can calculate the current semester for the selected students for the current academic cycle. The semester calculation can only be performed for active students.",
            "CompletedWithErrors": {
              "Description": {
                "One": "The semester of one student was not calculated due to an error.",
                "Many": "The semester for {{errors}} students was not calculated due to errors."
              }
            }
          },
          "ChangeProgramAction": {
            "Description": "With this action, you can transfer the selected students to another active study program of the organizational unit. The action can be performed in bulk only for active students belonging to the same study program and following the same direction.",
            "CompletedWithErrors": {
              "Description": {
                "One": "One student was not transferred to the new study program due to an error.",
                "Many": "{{errors}} students were not transferred to the new study program due to errors."
              }
            },
            "NoItems": "The selected students must be active and belong to the same study program and direction."
          },
          "AddProgramGroupAction": {
            "Description": "With this action, you can add a program group to the selected students. The action can be performed in bulk only for active students belonging to the same study program.",
            "DescriptionSingleStudent": "With this action, you can add a program group to the student.",
            "CompletedWithErrors": {
              "Description": {
                "One": "The group was not added to a student either because it already exists or because the conditions are not met.",
                "Many": "The group was not added to {{errors}} students either because it already exists or because the conditions are not met."
              }
            },
            "NoItems": "The selected students must be active and belong to the same study program.",
            "NoGroups": "The study program followed by the students does not have groups to choose from.",
            "AlreadySubscribedError": "The student is already subscribed to the program group.",
            "NotActiveStudentError": "The student's status is not active."
          },
          "ChangeSpecialtyAction": {
            "Description": "With this action, you can transfer the selected students to a specialty of the study program. The action can be performed in bulk only for active students belonging to the same study program.",
            "CompletedWithErrors": {
              "Description": {
                "One": "One student was not transferred to the new specialty either because they already follow the new specialty or because they do not meet the admission requirements.",
                "Many": "{{errors}} students were not transferred to the new specialty either because they already follow the new specialty or because they do not meet the admission requirements."
              }
            },
            "NoItems": "The selected students must be active and belong to the same study program.",
            "NoSpecialties": "The study program followed by the students does not have specialties to choose from."
          },
          "AddDocumentRequest": {
            "Description": "With this action, you can create bulk document/certificate requests for the selected students only if the conditions are met."
          },
          "CheckScholarshipRulesAction": {
            "Description": "With this action, you can initiate the scholarship conditions check process for the selected students. Once the check is completed, you can proceed to the scholarship to view the results of the check."
          },
          "CheckGraduationRules": "Check Graduation Requirements",
          "CheckGraduationRulesAction": {
            "Title": "Check Graduation Requirements",
            "Description": "With this action, you can initiate the graduation requirements check process for the selected active students. Once the check is completed, you can proceed to 'Graduation Requirements Checks' to view the results of the check.",
            "CompletedWithErrors": {
              "Description": {
                "One": "View the check results in 'Graduation Requirements Checks'."
              }
            }
          },
          "ConvertToElotAction": {
            "Description": "With this action, you can fill in the 'Name', 'Surname', 'Father's Name', 'Mother's Name' fields of the selected students in Latin letters, following the ELOT-743 standard. The action can be performed for students whose at least one of the aforementioned information exists and is not already filled in Latin letters in the ELOT-743 format.",
            "CompletedWithErrors": {
              "Description": {
                "One": "The information of one student was not updated due to an error that occurred during the execution of the process.",
                "Many": "The information of {{errors}} students was not updated due to errors that occurred during the execution of the process."
              }
            }
          },
          "Registration": {
            "NewRegistrationMsg": "The student has no declaration for the current academic cycle ({{year}} {{period}}).",
            "NewModalMsg": "You are about to create an empty declaration for the student. Do you want to continue?",
            "NewRegistrationDsc": "You are about to create an empty declaration for the student. Select academic year-cycle.",
            "NotActiveMsg": "A new declaration cannot be created because the student is not active."
          },
          "AddStudentCounselorAction": {
            "Description": "With this action, you can assign an academic advisor to the selected students.",
            "CompletedWithErrors": {
              "Description": {
                "One": "A counselor-student connection was not created due to an error.",
                "Many": "{{errors}} counselor-student connections were not created due to errors."
              }
            }
          },
          "DepartmentSnapshotDescription": "There is a student connection with an organizational unit change history with description:",
          "AddDepartmentSnapshotAction": {
            "Description": "With this action, you can link the selected students with an organizational unit history so that changes in the organizational unit are available to the student."
          },
          "RemoveDepartmentSnapshotAction": {
            "Message": "You are about to unlink the student from the organizational unit change history. Do you want to continue?",
            "Success": "The student no longer has a connection with any organizational unit change history."
          },
          "RemoveInfoTitle": "Delete student Information",
          "StudentGraduationRulesHelp": "Special graduation rules have been defined. Click 'Edit Rules' to change them or 'Delete Rules' to revert to the study program rules followed by the student.",
          "RemoveRulesMessage": "You are about to delete all special graduation rules defined for the student. After deletion, the graduation rules defined in the study program followed by the student will apply. Do you want to continue?",
          "CopyRules": {
            "SuccessMessage": "The graduation rules of the study program were successfully copied as special graduation rules of the student.",
            "DialogMessage": "You are about to copy the graduation rules of the study program as special graduation rules of the student. Do you want to continue?"
          },
          "StudyProgramRules": "The graduation rules of the study program followed by the student apply. Click 'Define Special Rules' if you want...",
          "Attachments": {
            "Info": "Manage the attachments of the student. You can preview/download existing attachments and upload new ones by selecting 'Upload Attachment'. You can also delete existing attachments, provided that they were uploaded by you (see 'Uploaded by' field)."
          },
          "AddStudentCourse": {
            "Success": {
              "Message": "The selected courses have been successfully added to the student's tab."
            }
          },
          "ProgramGroupsOverview": {
            "AddedSuccessfully": {
              "Description": "\r\nThe groups have been successfully added to the student.\r\n"
            },
            "FailedToAdd": {
              "Description": "\r\nThe groups were not added to the student.\r\n"
            },
            "GeneralError": {
              "Description": "\r\nAn error occurred while updating the student's groups. Please try again."
            }
          },
          "RemoveStudentProgramGroupTitle": "Remove student program group",
          "GraduationTypes": [
            "Graduation Completion"
          ],
          "GraduationInformationEdit": "Graduation information edit",
          "ChangeStatusAction": {
            "Title": "Restore students to active status",
            "Description": "With this action, you can restore selected students to active status. Students must be deleted, declared, or graduated. After completing the action, all related information from their previous status will be deleted (e.g., diploma grade for graduates, deletion date for deleted students, etc.). Note: If you wish to restore students who are suspended, you can specify their reinstatement in the suspension details (in the student view).",
            "CompletedWithErrors": {
              "Description": {
                "One": "One student could not be restored to active status due to an error.",
                "Many": "{{errors}} students could not be restored to active status due to errors."
              }
            }
          },
          "CreateUserAction": {
            "Description": "With this action, you can create users for selected students. Usernames will be formatted according to the specified configuration. This action can be performed for students who are not already associated with a user.",
            "SettingsRequiredHelp": "To use the user creation action, you must first define the appropriate formatting. Refer to the 'Other properties' tab on the organizational unit editing page and complete the 'Student username formatting' field."
          },
          "UnlinkUserAction": {
            "Description": "With this action, you can unlink selected students from their users (if they exist). Note that users will not be deleted; they will simply no longer be linked to the respective student.",
            "CompletedWithErrors": {
              "Description": {
                "One": "One student-user link deletion failed due to an error that occurred during the process.",
                "Many": "{{errors}} student-user link deletions failed due to an error that occurred during the process."
              }
            }
          },
          "ConvertToGenitiveAction": {
            "Description": "With this action, you can convert the father's and mother's names of selected students to genitive case. This action can be performed for students for whom at least one of the aforementioned names is empty. Note that already completed names are not affected by the process; for example, if a student's father's name is already in genitive case but the mother's name is not, only the latter will be completed.",
            "CompletedWithErrors": {
              "Description": {
                "One": "The father/mother names of one student were not converted to genitive case due to an error that occurred during the process.",
                "Many": "The father/mother names of {{errors}} students were not converted to genitive case due to an error that occurred during the process."
              }
            }
          },
          "CalculateGradeWrittenInWordsAction": {
            "Description": "With this action, the diploma grade in words can be calculated and automatically completed for selected students. This is only done for graduate students who do not already have the diploma grade completed in words.",
            "CompletedWithErrors": {
              "Description": {
                "One": "The diploma grade in words for one student was not completed due to an error that occurred during the process.",
                "Many": "The diploma grades in words for {{errors}} students were not completed due to an error that occurred during the process."
              }
            }
          },
          "CompleteDeclarationInfo": {
            "Modal": {
              "Message": "You are about to complete the declaration of the student and change their status to graduate. It is recommended to check and edit their declaration information before proceeding with this action. Do you wish to continue?",
              "RequestExists": "There is an active graduation request for the specific student, therefore you must complete their declaration from that point. Would you like to be redirected to the request page?"
            },
            "Toast": {
              "Message": "The student's declaration has been successfully completed."
            }
          },
          "CancelDeclarationInfo": {
            "Modal": {
              "Message": "You are about to cancel the declaration of the student and revert them to active status. All related declaration information will be deleted. Do you wish to continue?"
            },
            "Toast": {
              "Message": "The cancellation of the student's declaration has been successfully completed."
            }
          },
          "CancelGraduation": "Cancel graduation",
          "CancelGraduationInfo": {
            "Modal": {
              "Title": "Cancel graduation",
              "Message": "You are about to cancel the graduation of the student and revert them to declared status. Do you wish to continue?"
            },
            "Toast": {
              "Title": "Cancel graduation",
              "Message": "The cancellation of the student's graduation has been successfully completed."
            }
          },
          "DeleteSuspensionAction": {
            "ModalMessage": "You are about to permanently delete the suspension of the student and revert them to active status. In case of reinstatement, prefer the action in the suspension editing. Do you wish to continue?",
            "ToastMessage": "The student's suspension has been deleted successfully and the student has been restored to active status."
          },
          "DeleteAction": {
            "Title": "Delete students",
            "Description": "With this action, you can permanently delete the selected students. Deletion can only be performed for active students who do not have associated actions (requests, declarations, grades, etc.).",
            "CompletedWithErrors": {
              "Description": {
                "One": "One student was not deleted either due to an error or because there are associated actions.",
                "Many": "{{errors}} students were not deleted either due to errors or because there are associated actions."
              }
            }
          }
        },
        "StudyPrograms": {
          "GraduateCalculationRules": "Definition of grade calculation method",
          "DegreeDescription": "Title of studies",
          "StatisticsSpecialtiesActiveStudents": "Statistics - Active students by specialization",
          "TotalActiveStudents": "All active students",
          "ActiveStudents": "Active students",
          "NumberOfStudents": "Number of students",
          "TotalStudents": "All students",
          "Students": "Students",
          "StatisticsStudentStatus": "Statistics - Student status in the study program",
          "RemoveSpecialtyMessage": "You are about to delete one or more specializations of the study program. Deletion of a specialization can only be done if there are no associated students or courses. Do you want to proceed?",
          "RemoveGroupMessage": "You are about to delete one or more groups of the study program. Deletion of a group can only be done if there are no associated students or courses. Do you want to proceed?",
          "DeleteProgram": {
            "ModalWarningCannotDelete": {
              "Students": "The study program cannot be deleted because it is followed by at least one student."
            }
          },
          "AddCourse": "Add teaching unit"
        },
        "Theses": {
          "FullΝame": "Student Full Name",
          "StudentFamilyName": "Student Last Name",
          "StudentFullName": "Student Full Name",
          "PreviewStudent": "Preview student",
          "Students": "Students",
          "Semester": "Student Semester",
          "StudentInfo": "Student Information",
          "NoStudents": "No students available",
          "Student": "Student",
          "AddStudentsMessage": {
            "title": "Add student to thesis",
            "one": "One new student added successfully.",
            "many": "{{value}} new students added successfully."
          },
          "RemoveStudentsMessage": {
            "title": "Remove student from thesis",
            "one": "One student removed successfully.",
            "many": "{{value}} students removed successfully."
          },
          "RemoveStudentTitle": "Remove student",
          "RemoveStudentMessage": "You are about to delete one or more students from the thesis. Deletion can only be done for active students who have not been graded. Do you want to proceed?",
          "AddStudent": "Add student",
          "StudentId": "Student ID",
          "StudentStatus": "Student Status",
          "StudentStatuses": {
            "candidate": "Pre-registration student"
          },
          "DeleteAction": {
            "Description": "With this action, you can delete a thesis. Deletion is possible for theses that are not completed or assigned to students.",
            "CannotDelete": {
              "Description": "The thesis has been assigned to students.",
              "Student": "Deleting inactive students from the thesis is not allowed."
            }
          }
        },
        "Sidebar": {
          "Students": "Students",
          "Candidates": "Candidate students",
          "CheckGraduationRules": "Check graduation requirements",
          "CandidateStudentUploadActions": "Student import actions",
          "RestoreDeletedStudents": "Restore deleted students",
          "RestoreStudents": "Restore students"
        },
        "StudentStatuses": {
          "candidate": "Candidate student"
        },
        "StudentRequestActionsTypes": {
          "GraduationRequestAction": "Graduation request",
          "GraduationRequestActions": "Graduation requests"
        },
        "AutoRegistrationStatus": {
          "student": "By the student"
        },
        "Spotlight": {
          "students": "Students",
          "search": "Search for students and teachers",
          "noStudentsFound": "No students found"
        },
        "student": "Student",
        "AdvancedSearch": {
          "PreDefinedLists": {
            "firstYearStudents": "First-year students",
            "activeStudents": "Active students",
            "declaredStudents": "Declared students",
            "graduatedStudents": "Graduated students",
            "suspendedStudents": "Suspended students",
            "erasedStudents": "Erased students"
          }
        },
        "AutoRegisterStatusTypes": {
          "student": "By the student"
        },
        "StudentColumns": {
          "graduationDate": "Graduation date",
          "graduationPeriodAlternateName": "Graduation cycle",
          "graduationYearName": "Graduation year",
          "graduationTypeName": "Graduation type",
          "graduationRankByGraduationYear": "Rank by graduation year"
        }
}
export {
    en
}