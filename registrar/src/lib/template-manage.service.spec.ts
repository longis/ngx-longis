import { TestBed } from '@angular/core/testing';

import { TemplateManageService } from './template-manage.service';

describe('TemplateManageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TemplateManageService = TestBed.get(TemplateManageService);
    expect(service).toBeTruthy();
  });
});
