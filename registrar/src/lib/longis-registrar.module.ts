import { CommonModule } from '@angular/common';
import { Injector, ModuleWithProviders, NgModule } from '@angular/core';
import { Route, NavigationEnd,  Router, RouterModule, Routes } from '@angular/router';
import { el, en } from './i18n';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { ActivatedUser, APP_LOCATIONS, AppEventService, ApplicationConfiguration, ConfigurationService, SharedModule } from '@universis/common';
import { REGISTRAR_APP_LOCATIONS } from './app.locations';
import { TemplateManageService } from './template-manage.service';
import { AngularDataContext, MostModule } from '@themost/angular';

function removeRoute(config: Routes, path: string): Route | undefined  {
  const commands = path.split('/');
  let index = 0;
  // get root
  let currentRoute = config.find((item) => item.path === '' && item.redirectTo == null);
  let currentRoutes: Routes = [];
  while (index < commands.length) {
    const loadedConfig: { routes: Routes } = (currentRoute as any)._loadedConfig;
    if (loadedConfig) {
      currentRoutes = loadedConfig.routes
    } else if (currentRoute.children) {
      currentRoutes = currentRoute.children
    }
    // try to get current route
    currentRoute = currentRoutes.find((item) => item.path === commands[index]);
    if (currentRoute == null) {
      return;
    }
    index += 1;
  }
  if (currentRoute) {
    const removeIndex = currentRoutes.findIndex((item) => item === currentRoute);
    if (removeIndex >= 0) {
      currentRoutes.splice(removeIndex, 1);
      return currentRoute;
    }
  }
}

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    TranslateModule,
    MostModule,
    RouterModule
  ],
  declarations: [],
  exports: []
})
export class LongisRegistrarModule {
  constructor(private router: Router, private translate: TranslateService, private configuration: ConfigurationService, private activatedUser: ActivatedUser, private injector: Injector, private appEvent: AppEventService) {
    router.events.subscribe((routerEvent: any) => {
      if (routerEvent instanceof NavigationEnd) {
        const paths = [
          'students/:id/dashboard/courses',
          'students/:id/dashboard/scholarships',
          'students/:id/dashboard/internships',
          'students/:id/dashboard/informations',
          'students/:id/dashboard/theses',
          'study-programs/:id/preview/semesterRules',
          'study-programs/:id/preview/groups',
          'study-programs/:id/preview/specialties'
        ];
        paths.forEach((path) => removeRoute(router.config, path));
      }
    });

    this.appEvent.add.subscribe((event?: { service: TranslateService | any, type: any, target?: any }) => {
      if (event && event.service instanceof TranslateService && event.type === this.translate.setTranslation) {
        this.translate.setTranslation('el', el, true);
        this.translate.setTranslation('en', en, true);
      }
    });
    this.configuration.loaded.subscribe((config: ApplicationConfiguration) => {
      if (config) {
        setTimeout(() => {
          this.activatedUser.user.subscribe((user) => {
            if (user) {
              const context = this.injector.get<AngularDataContext>(AngularDataContext);
              context.model('AcademicPeriods').asQueryable().expand('locale').take(-1).getItems().then((periods) => {
                const translations = periods.map((period: any) => {
                  const key = period.alternateName + 'Long';
                  return {
                    [period.alternateName]: (period.locale && period.locale.name) || period.alternateName,
                    [key]: (period.locale && period.locale.name) || period.alternateName
                  }
                }).reduce((prev: { Periods: any}, current: any) => {
                  Object.assign(prev.Periods, current);
                  return prev;
                }, {
                  Periods: {}
                });
                this.translate.setTranslation(this.translate.currentLang, translations, true);
              });
            }
          });
        });
      }
    });

  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: LongisRegistrarModule,
      providers: [{
        provide: APP_LOCATIONS,
        useValue: REGISTRAR_APP_LOCATIONS
      }, 
      TemplateManageService]
    }
  }

}
