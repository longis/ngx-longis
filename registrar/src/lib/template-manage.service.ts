import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

export class TemplateManageService {
  
  private currentStudyProgramSubject = new BehaviorSubject(null);
  currentStudyProgram = this.currentStudyProgramSubject.asObservable();

  private show = new BehaviorSubject(true);
  toggleShow = this.show.asObservable();

  private showRouterOutlet = new BehaviorSubject(false);
  toggleShowRouterOutlet = this.showRouterOutlet.asObservable();

  private studyProgram: string;
  // longis uses only one specialization equal to -1
  private specialization: number = -1;

  constructor() {
   }

    setCurrentStudyProgram(studyProgram: number) {
      return this.currentStudyProgramSubject.next(studyProgram);
    }

    updateShowStatus(show: boolean) {
      return this.show.next(show);
    }

    updateShowRouterOutletStatus(showRouterOutlet: boolean) {
      return this.showRouterOutlet.next(showRouterOutlet);
    }

    globalNavigationCorrectionService (segment?: string ) {
      this.currentStudyProgram.subscribe((studyProgram) => this.studyProgram = studyProgram.toString());
      const route = `/study-programs/${encodeURIComponent(this.studyProgram)}${segment ? '/' + segment +'/': '/'}specialization/${encodeURIComponent(this.specialization.toString())}`
      return route;
    }



}
